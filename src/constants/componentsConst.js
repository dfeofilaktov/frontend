// FOOTER
export const FOOTER_BOX = 'BOX';
export const FOOTER_RADIO = 'RADIO';
export const FOOTER_LOGIN = 'LOGIN';
export const FOOTER_RISIN = 'RISIN';
export const FOOTER_RIVERSIDE = 'RIVERSIDE';
export const FOOTER_LIGHTHOUSE = 'LIGHTHOUSE';

// MENU
export const MENU_HOME = 'Home';
export const MENU_PORTFOLIO = 'PORTFOLIO';
export const MENU_TEAM = 'TEAM';

export const TOPIC_WEB = 'web';
export const TOPIC_ILLUSTRATIONS = 'illustrations';

export const ADOO_BUTTON_ICON = 'adooicon';
