import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
    palette: {
        primary: {
            // light: will be calculated from palette.primary.main,
            main: '#2fc686',
            // dark: will be calculated from palette.primary.main,
            // contrastText: will be calculated to contrast with palette.primary.main
        },
        secondary: {
            // light: '#6ad1c5',
            main: '#6ad1c5',
            // dark: will be calculated from palette.secondary.main,
            // contrastText: '#ffcc00',
        },
    },
});

export default theme;
