import globalHook from 'use-global-hook';
import Immutable from 'seamless-immutable';
import { version } from '../../package.json';
import {
    MENU_HOME,
    // MENU_PORTFOLIO,
} from '../constants/componentsConst';

const initialState = Immutable({
    loading: true,
    selectedMenu: MENU_HOME,
    // selectedMenu: MENU_PORTFOLIO,
    selectedTopic: null,
    isAuthorizationOpened: false,
    setLogged: false,
    user: {},
    app: {
        front: {
            version,
        },
        core: {
            version: '',
        },
    },
    projects: [],
});

const actions = {
    setAppLoading: (store, value) => {
        store.setState({ loading: value });
    },
    selectMenuItem: (store, value) => {
        store.setState({ selectedMenu: value });
    },
    setAuthorizationOpen: (store, value) => {
        store.setState({ isAuthorizationOpened: value });
    },
    setLogged: (store, value) => {
        store.setState({ setLogged: value });
    },
    setCoreData: (store, value) => {
        const newState = _.cloneDeep(store.state);
        newState.app.core = value;
        store.setState(newState);
    },
    setProjects: (store, value) => {
        store.setState({ projects: value });
    },
    setTopic: (store, value) => {
        store.setState({ selectedTopic: value });
    },
};

const useGlobal = globalHook(React, initialState, actions);

export default useGlobal;
