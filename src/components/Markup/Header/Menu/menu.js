// import MenuItem from './MenuItem/menu_item';
// import { useState } from 'react';
import { A } from 'hookrouter';
import AdooButton from '../../../CustomShop/AdooButton/adoo_button';
import useGlobal from '../../../../hooks/store';
import { MENU_HOME, MENU_PORTFOLIO, MENU_TEAM } from '../../../../constants/componentsConst';
// import MobileMenu from './mobileMenu/mobileMenu';

// import PropTypes from 'prop-types';
// import classnames from 'classnames';

const Menu = () => {
    const [, actions] = useGlobal();
    // const [mobileMenuVisible, setMobileMenuVisible] = useState(false);
    return (
        <div className='menu'>
            <div className='items'>
                <A href='/'>
                    <AdooButton
                        color='inherit'
                        className='home_icon'
                        icon
                        onClick={() => actions.selectMenuItem(MENU_HOME)}
                    >
                        home
                    </AdooButton>
                </A>
                <A href='/portfolio'>
                    <AdooButton
                        bordered
                        index={0}
                        caption='PORTFOLIO'
                        onClick={() => {
                            actions.selectMenuItem(MENU_PORTFOLIO);
                        }}
                    />
                </A>
                <A href='/team'>
                    <AdooButton
                        bordered
                        index={1}
                        caption='TEAM'
                        onClick={() => actions.selectMenuItem(MENU_TEAM)}
                    />
                </A>
            </div>
            {/* <AdooButton
                icon
                className='menu_icon'
                onClick={() => {
                    setMobileMenuVisible(true);
                }}
            >
                menu
            </AdooButton>
            <MobileMenu
                open={mobileMenuVisible}
                onSelectMenu={() => setMobileMenuVisible(false)}
            /> */}
        </div>
    );
};

Menu.propTypes = {};

Menu.defaultProps = {};

export default Menu;
