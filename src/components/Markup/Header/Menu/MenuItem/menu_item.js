import PropTypes from 'prop-types';


const MenuItem = (props) => {
    const { name } = props;
    return (
        <div className='menu_item'>
            {name}
        </div>
    );
};

MenuItem.propTypes = {
    name: PropTypes.string,
};

MenuItem.defaultProps = {
    name: 'item',
};

export default MenuItem;
