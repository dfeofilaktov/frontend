import PropTypes from 'prop-types';
import classnames from 'classnames';

import MenuDescription from '../../../../../meta/menu';
import AdooButton from '../../../../CustomShop/AdooButton/adoo_button';
import useGlobal from '../../../../../hooks/store';

const MobileMenu = ({ open, onSelectMenu }) => {
    const [, actions] = useGlobal();

    return (
        <div className={classnames('MobileMenu', { open })}>
            {_.map(MenuDescription, (item, i) => (
                <AdooButton
                    bordered
                    key={i}
                    className='mobile_menu_item'
                    onClick={() => {
                        actions.selectMenuItem(item.name);
                        onSelectMenu();
                    }}
                >
                    {item.name}
                </AdooButton>
            ))}
        </div>
    );
};

MobileMenu.propTypes = {
    open: PropTypes.bool,
    onSelectMenu: PropTypes.func,
};

MobileMenu.defaultProps = {
    open: false,
    onSelectMenu: () => {},
};

export default MobileMenu;
