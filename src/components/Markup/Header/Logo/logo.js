import { LOGO_SRC } from '../../../../constants/images';
// import {
//     CORE_HOST_LOCAL,
//     CORE_HOST_STAGE,
//     CORE_HOST_PROD,
// } from '../../../../constants/environment';

/* eslint-disable react/jsx-indent-props */

const Logo = () => {
    return (
        <div
            role='presentation'
            className='logo'
        >
            <div className='ooosvg'>
                <img src={LOGO_SRC} alt='logo' />
            </div>
            {/* {process.env.CORE_HOST === CORE_HOST_LOCAL && 'Adooone Local'}
            {process.env.CORE_HOST === CORE_HOST_STAGE && 'Adooone Stage'}
            {process.env.CORE_HOST === CORE_HOST_PROD && 'Adooone'} */}
            Adooone
        </div>
    );
};

export default Logo;
