// import PropTypes from 'prop-types';
// import classnames from 'classnames';
import Menu from './Menu/menu';
import Logo from './Logo/logo';

const Header = () => {
    return (
        <header>
            <div className='header_box'>
                <Logo />
                <Menu />
            </div>
        </header>
    );
};

Header.propTypes = {};

Header.defaultProps = {};

export default Header;
