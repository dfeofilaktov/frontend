// import PropTypes from 'prop-types';
// import classnames from 'classnames';

const Slogan = () => {
    return (
        <div className='slogan'>
            <div className='compname'>ADOOONE WEB PRODUCTION</div>
            <div className='comdslog'>&nbsp;GOOD TIMES</div>
        </div>
    );
};

Slogan.propTypes = {};

Slogan.defaultProps = {};

export default Slogan;
