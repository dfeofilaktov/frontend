import classnames from 'classnames';
import PropTypes from 'prop-types';
import { useState } from 'react';


const FooterItem = (props) => {
    const { caption, onClick } = props;
    const [clicked, setClicked] = useState();
    const handleClick = () => {
        setClicked(true);
        setTimeout(() => setClicked(false), 1000);
        onClick();
    };
    return (
        <div
            role='presentation'
            className={classnames('ft_item', { clicked })}
            onClick={handleClick}
        >
            <div className='effect' />
            <div className='effect2' />
            <div className='effect3' />
            {caption}
        </div>
    );
};

FooterItem.propTypes = {
    caption: PropTypes.string,
    onClick: PropTypes.string,
};

FooterItem.defaultProps = {
    caption: 'item',
    onClick: () => {},
};

export default FooterItem;
