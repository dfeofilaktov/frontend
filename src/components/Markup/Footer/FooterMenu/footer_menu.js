// import FooterItem from './FooterItem/footer_item';
import useGlobal from '../../../../hooks/store';
import FooterDescription from '../../../../meta/footer';
import AdooButton from '../../../CustomShop/AdooButton/adoo_button';

// import PropTypes from 'prop-types';
// import classnames from 'classnames';

const FooterMenu = () => {
    const [store, actions] = useGlobal();
    const handleClick = () => {
        if (store.setLogged) {
            actions.setLogged(false);
        } else {
            // actions.setAuthorizationOpen(true);
        }
    };
    return (
        <div className='footer'>
            <div className='ft_items'>
                {
                    _.map(FooterDescription, (item, i) => (
                        <AdooButton
                            key={i}
                            bordered
                            caption={item.caption}
                            onClick={handleClick}
                        />
                    ))
                }
            </div>
        </div>
    );
};

FooterMenu.propTypes = {};

FooterMenu.defaultProps = {};

export default FooterMenu;
