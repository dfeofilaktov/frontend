import FooterMenu from './FooterMenu/footer_menu';
import Slogan from './Slogan/slogan';
import useGlobal from '../../../hooks/store';
import { CORE_HOST_LOCAL, CORE_HOST_STAGE, CORE_HOST_PROD } from '../../../constants/environment';

// import PropTypes from 'prop-types';
// import classnames from 'classnames';

const Footer = () => {
    const [store] = useGlobal();
    const appInfo = () => (
        <div className='appdata'>
            <div>
                <div className='appname'>frontend</div>
                <div className='version'>{`v${store.app.front.version}`}</div>
                {process.env.CORE_HOST === CORE_HOST_LOCAL && <div className='environment'>local env</div>}
                {process.env.CORE_HOST === CORE_HOST_STAGE && <div className='environment'>stage env</div>}
            </div>
            <div>
                <div className='appname'>backend</div>
                {store.app.core.version
                    ? <div className='version'>{`v${store.app.core.version}`}</div>
                    : <div className='version'>not connected</div>}
            </div>
        </div>
    );
    return (
        <footer>
            <div className='footer_box'>
                <Slogan />
                <FooterMenu />
            </div>
            {process.env.CORE_HOST !== CORE_HOST_PROD && appInfo()}
        </footer>
    );
};

Footer.propTypes = {};

Footer.defaultProps = {};

export default Footer;
