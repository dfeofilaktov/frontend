// import PropTypes from 'prop-types';
import useGlobal from '../../hooks/store';

import '../../sass/main.sass';
import Header from './Header/header';
import Footer from './Footer/footer';
import Authorization from '../Helpers/Authorision/authorization';
import Content from './Content/content';

const Markup = () => {
    const [store] = useGlobal();
    return (
        <div className='markup'>
            <Authorization open={store.isAuthorizationOpened} />
            <Header />
            <Content />
            <Footer />
        </div>
    );
};

Markup.propTypes = {};

Markup.defaultProps = {};

export default Markup;
