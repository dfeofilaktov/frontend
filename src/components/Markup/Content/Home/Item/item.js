import PropTypes from 'prop-types';
import { useState } from 'react';
import AdooButton from '../../../../CustomShop/AdooButton/adoo_button';
// import classnames from 'classnames';

const Item = ({ data, remove, check }) => {
    const [checked, setChecked] = useState(false);
    return (
        <div className='Item'>
            <p>{`${data.id + 1}. ${data.name}`}</p>
            <div className='itemControls'>
                <AdooButton
                    // color='inherit'
                    icon
                    onClick={() => {
                        setChecked(!checked);
                        check(data.id, !checked);
                    }}
                >
                    {data.checked ? 'check_box' : 'check_box_outline_blank'}
                </AdooButton>
                <AdooButton
                    // color='inherit'
                    className='rmvBtn'
                    icon
                    color='#aa3311'
                    onClick={() => {
                        remove(data.id);
                    }}
                >
                    delete_forever
                </AdooButton>
            </div>
        </div>
    );
};

Item.propTypes = {
    data: PropTypes.object.isRequired,
    remove: PropTypes.func.isRequired,
    check: PropTypes.func.isRequired,
};

Item.defaultProps = {};

export default Item;
