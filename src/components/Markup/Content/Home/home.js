// import PropTypes from 'prop-types';
import classnames from 'classnames';
import { useEffect } from 'react';

const Home = () => {
    const didMount = () => {
        // coreone.getList()
        //     .then((response) => {
        //         const [{ goods }] = response.data.dr;
        //         setList(goods);
        //     });
    };
    useEffect(didMount, []);

    return (
        <div className={classnames('Home', { hidden: false })}>
            <div className='central_text'>Hello world</div>
        </div>
    );
};

Home.propTypes = {
    // open: PropTypes.bool,
};

Home.defaultProps = {
    // open: false,
};

export default Home;
