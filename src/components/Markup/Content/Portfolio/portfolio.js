// import PropTypes from 'prop-types';
import classnames from 'classnames';
import { useEffect, useState, useRef } from 'react';
// import anime from 'animejs';

import topics from '../../../../meta/projects';
// import coreone from '../../../../api/core';
import useGlobal from '../../../../hooks/store';
import ImageView from './image/image';
import InfoView from './info/info';
import Controls from './controls/controls';
import WebSites from './web/webSites';

const Portfolio = () => {
    // if (!open) return null;
    const topicsContainer = useRef(null);
    const imgs = useRef(null);
    const infos = useRef(null);

    const [store, actions] = useGlobal();
    const [imgsLoaded, setImgsLoaded] = useState([]);
    const [activeIndex, setActive] = useState(0);

    useEffect(() => {
        actions.setTopic(null);
    }, []);

    useEffect(() => {
        imgs.current.style.transform = `translateY(${activeIndex * (-68)}vh)`;
        infos.current.style.transform = `translateY(${activeIndex * (-32)}vh)`;
    }, [activeIndex]);

    useEffect(() => {
        if (store.selectedTopic) {
            topicsContainer.current.style.transform = 'translateX(-400px)';
            topicsContainer.current.style.opacity = '0';
        } else {
            topicsContainer.current.style.transform = 'translateX(0) scale(1)';
            topicsContainer.current.style.opacity = '1';
        }
    }, [store.selectedTopic]);

    const onImgLoad = (index) => {
        const loaded = imgsLoaded;
        loaded.push(index);
        setImgsLoaded(loaded);
    };

    return (
        <div
            className={classnames('Portfolio', { hidden: false })}
            // onWheel={}
        >
            <div className='topics' ref={topicsContainer}>
                <div className='topic_images' ref={imgs}>
                    {_.map(topics, (item, i) => (
                        <ImageView
                            item={item}
                            active={activeIndex === i}
                            onImgLoad={onImgLoad}
                            index={i}
                            key={i}
                        />
                    ))}
                </div>
                <div className='topic_infos' ref={infos}>
                    {_.map(topics, (item, i) => (
                        <InfoView
                            item={item}
                            active={activeIndex === i}
                            index={i}
                            key={i}
                        />
                    ))}
                </div>
            </div>
            <Controls
                setActive={(step) => { setActive(activeIndex + step); }}
                disabled={imgsLoaded.length !== topics.length}
            />
            <WebSites open={store.selectedTopic} />
        </div>
    );
};

Portfolio.propTypes = {
    // open: PropTypes.bool,
};

Portfolio.defaultProps = {
    // open: false,
};

export default Portfolio;
