import PropTypes from 'prop-types';
import classnames from 'classnames';
// import anime from 'animejs';
import { useState } from 'react';

const ImageView = (props) => {
    const {
        item,
        index,
        active,
        onImgLoad,
    } = props;

    const [loaded, setLoaded] = useState(false);

    const onLoad = () => {
        onImgLoad(index);
        setTimeout(() => setLoaded(true), index * 1000 + 100);
    };

    return (
        <div
            className={classnames('project_image', {
                active,
                non_active: !active,
                loaded,
            })}
            key={index}
        >
            <img
                onLoad={onLoad}
                src={item.img}
                alt={item.name}
            />
        </div>
    );
};

ImageView.propTypes = {
    item: PropTypes.object.isRequired,
    active: PropTypes.bool.isRequired,
    onImgLoad: PropTypes.func.isRequired,
    index: PropTypes.number.isRequired,
};

ImageView.defaultProps = {};

export default ImageView;
