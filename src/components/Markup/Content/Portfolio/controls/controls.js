import PropTypes from 'prop-types';
import classnames from 'classnames';

import AdooButton from '../../../../CustomShop/AdooButton/adoo_button';

const Controls = ({ setActive, disabled }) => {
    return (
        <div className={classnames('proj_controls', { disabled })}>
            <AdooButton
                className='btn'
                contained
                onClick={() => setActive(-1)}
            >
                up
            </AdooButton>
            <AdooButton
                className='btn'
                contained
                onClick={() => setActive(1)}
            >
                down
            </AdooButton>
        </div>
    );
};

Controls.propTypes = {
    setActive: PropTypes.func,
    disabled: PropTypes.bool,
};

Controls.defaultProps = {
    setActive: () => {},
    disabled: false,
};

export default Controls;
