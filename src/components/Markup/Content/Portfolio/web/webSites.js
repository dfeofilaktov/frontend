import PropTypes from 'prop-types';
// import classnames from 'classnames';

import { useEffect, useRef, useState } from 'react';
import coreone from '../../../../../api/core';

const WebSites = ({ open }) => {
    if (!open) return null;

    const web = useRef();
    const [projects, setProjects] = useState([]);

    const didOpen = () => {
        coreone.getProjects()
            .then((response) => {
                const { projects: proj } = response.data;
                setProjects(proj);
                web.current.style.transform = 'translateX(0)';
                web.current.style.opacity = '1';
            });
    };
    useEffect(didOpen, [open]);

    return (
        <div className='Web' ref={web}>
            {_.map(projects, (item, i) => (
                <div className='project' key={i}>
                    <div className='proj_logo'>
                        <img src={item.logo} alt='proj_logo' />
                    </div>
                    <div className='proj_info'>{item.info}</div>
                    <div className='tablet'>
                        <img src='https://i.imgur.com/BS9X4De.png' alt='tablet' />
                    </div>
                </div>
            ))}
        </div>
    );
};

WebSites.propTypes = {
    open: PropTypes.any,
};

WebSites.defaultProps = {
    open: false,
};

export default WebSites;
