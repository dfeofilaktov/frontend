import PropTypes from 'prop-types';
// import classnames from 'classnames';

import classnames from 'classnames';
import { useState } from 'react';
import AdooButton from '../../../../CustomShop/AdooButton/adoo_button';
import useGlobal from '../../../../../hooks/store';

const InfoView = (props) => {
    const [, actions] = useGlobal();
    const [loaded, setLoaded] = useState(false);
    const {
        active, item, index,
    } = props;

    setTimeout(() => {
        setLoaded(true);
    }, index * 1000 + 500);

    return (
        <div
            className={classnames('project_info', {
                active,
                non_active: !active,
                loaded,
            })}
            key={index}
        >
            <div className='name'>{item.name}</div>
            <div className='info'>{item.description}</div>
            <AdooButton
                contained
                className='open_btn'
                caption='Open'
                onClick={() => actions.setTopic(item.topic)}
            />
        </div>
    );
};

InfoView.propTypes = {
    active: PropTypes.bool.isRequired,
    item: PropTypes.object.isRequired,
    index: PropTypes.number.isRequired,
};

InfoView.defaultProps = {};

export default InfoView;
