import { useRoutes } from 'hookrouter';

import Home from './Home/home';
import Portfolio from './Portfolio/portfolio';
import Team from './Team/team';
import NotFoundPage from './NotFoundPage/notFoundPage';

const HomeRoute = () => <Home />;
const PortfolioRoute = () => <Portfolio />;
const TeamRoute = () => <Team />;

const routes = {
    '/': HomeRoute,
    '/portfolio': PortfolioRoute,
    '/team': TeamRoute,
};

const Content = () => {
    return (
        <div className='Content'>
            {useRoutes(routes) || <NotFoundPage />}
        </div>
    );
};

Content.propTypes = {};

Content.defaultProps = {};

export default Content;
