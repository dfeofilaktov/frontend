// import PropTypes from 'prop-types';
import classnames from 'classnames';

const Team = () => {
    return (
        <div className={classnames('Team', { hidden: false })}>
            <div className='central_text'>
                Team
            </div>
        </div>
    );
};

Team.propTypes = {
    // open: PropTypes.bool,
};

Team.defaultProps = {
    // open: false,
};

export default Team;
