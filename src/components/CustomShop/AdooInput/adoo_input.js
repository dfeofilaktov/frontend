/* eslint-disable no-mixed-operators */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Icon } from '@material-ui/core';

const AdooInput = (props) => {
    const {
        icon,
        fakeText,
        inputType,
        onChange,
        // aaa,
        id,
    } = props;
    const [focused, setFocused] = useState(false);
    const [value, setValue] = useState('');
    return (
        <div
            role='presentation'
            className='adoo_input'
            onFocus={() => setFocused(true)}
            onBlur={() => setFocused(false)}
        >
            <div className='fake_text'>
                {focused || value !== '' ? '' : fakeText}
            </div>
            <input
                id={id}
                type={inputType}
                onChange={(e) => {
                    setValue(e.target.value);
                    onChange(e.target.value);
                }}
                // onClick={aaa(value, { id })}
            />
            <div className='icon'>
                <Icon
                    fontSize='small'
                >
                    { icon }
                </Icon>
            </div>
        </div>
    );
};

AdooInput.propTypes = {
    id: PropTypes.string.isRequired,
    icon: PropTypes.string.isRequired,
    fakeText: PropTypes.string.isRequired,
    inputType: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    // aaa: PropTypes.object.isRequired,
};

AdooInput.defaultProps = {};

export default AdooInput;
