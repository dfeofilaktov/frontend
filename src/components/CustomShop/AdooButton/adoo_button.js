import PropTypes from 'prop-types';
import { Icon } from '@material-ui/core';
import classnames from 'classnames';
import { useState } from 'react';

const AdooButton = (props) => {
    const {
        icon,
        bordered,
        contained,
        color,
        children,
        caption,
        onClick,
        className,
    } = props;
    const [clicked, setClicked] = useState();
    const handleClick = () => {
        onClick();
        setClicked(true);
        setTimeout(() => setClicked(false), 500);
    };
    const iconBtn = () => (
        <div
            role='presentation'
            className={classnames('adoo_button_icon', className)}
            onClick={onClick}
        >
            <Icon color={color}>
                {children}
            </Icon>
        </div>
    );
    const borderedBtn = () => (
        <div
            role='presentation'
            className={classnames('bordered_btn', className, { clicked })}
            onClick={handleClick}
        >
            <div className='effect' />
            <div className='effect2' />
            <div className='effect3' />
            {caption || children}
        </div>
    );
    const containedBtn = () => (
        <div
            role='presentation'
            className={classnames('contained_btn', className, { clicked })}
            onClick={handleClick}
        >
            {caption || children}
        </div>
    );
    if (icon) return iconBtn();
    if (bordered) return borderedBtn();
    if (contained) return containedBtn();
    return (
        <div
            role='presentation'
            className='adoo_button'
            onClick={onClick}
        >
            {caption}
        </div>
    );
};

AdooButton.propTypes = {
    icon: PropTypes.bool,
    bordered: PropTypes.bool,
    contained: PropTypes.bool,
    children: PropTypes.string,
    color: PropTypes.string,
    caption: PropTypes.string,
    onClick: PropTypes.func,
    className: PropTypes.string,
};

AdooButton.defaultProps = {
    icon: false,
    bordered: false,
    contained: false,
    children: '',
    color: 'primary',
    caption: null,
    className: '',
    onClick: () => {},
};

export default AdooButton;
