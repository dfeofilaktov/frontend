import { useEffect } from 'react';
import { ThemeProvider } from '@material-ui/styles';
// import ReactCanvasNest from 'react-canvas-nest';

import '../sass/main.sass';
import Markup from './Markup/markup';
import Loader from './Helpers/Loader/loader';
import useGlobal from '../hooks/store';
import coreone from '../api/core';
import AppBackground from './Helpers/Background/appBackground';
import theme from '../material/theme';
import { MENU_HOME } from '../constants/componentsConst';
// import Pointer from './Helpers/Pointer/pointer';
import canvas from '../canvas/canvas';

const App = () => {
    // const [pointerVisible, setPointerVisible] = useState(false);
    const [store, actions] = useGlobal();
    // const canvasRef = useRef(null);
    const matcher = window.matchMedia('(prefers-color-scheme: dark)');

    const onMediaUpdate = () => {
        const lightSchemeIcon = document.querySelector('link#light-scheme-icon');
        const darkSchemeIcon = document.querySelector('link#dark-scheme-icon');

        if (matcher.matches) {
            lightSchemeIcon.remove();
            document.head.append(darkSchemeIcon);
        } else {
            document.head.append(lightSchemeIcon);
            darkSchemeIcon.remove();
        }
    };

    const didMount = () => {
        coreone.ping()
            .then((response) => {
                const { core } = response.data;
                // console.log(`coreone V${core.version}`);
                actions.setCoreData(core);
            });
        console.log(`CORE_HOST=${process.env.CORE_HOST}`);

        onMediaUpdate();
        // canvas.init({});
        // setTimeout(canvas.blow, 5000);

        // document.addEventListener('mouseleave', () => {
        //     setPointerVisible(false);
        // });

        // document.addEventListener('mouseenter', () => {
        //     setPointerVisible(true);
        // });

        window.onload = () => {
            matcher.addListener(onMediaUpdate);
            actions.setAppLoading(false);
        };
    };

    useEffect(didMount, []);
    return (
        <ThemeProvider theme={theme}>
            <AppBackground darken={store.selectedMenu !== MENU_HOME} />
            <Loader />
            <Markup />
            {/*<ReactCanvasNest*/}
            {/*    className='canvas_nest'*/}
            {/*    config={{*/}
            {/*        count: 70,*/}
            {/*        pointColor: '47, 198, 134',*/}
            {/*        lineColor: '47, 198, 134',*/}
            {/*        mouseDist: 30000,*/}
            {/*    }}*/}
            {/*    style={{ zIndex: 0 }}*/}
            {/*/>*/}
            <canvas id='canvas' />
            {/* <Pointer visible={pointerVisible} /> */}
        </ThemeProvider>
    );
};

App.propTypes = {};

export default App;
