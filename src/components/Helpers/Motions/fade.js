import PropTypes from 'prop-types';
import classnames from 'classnames';
import { useEffect, useState } from 'react';

const Fade = ({ children, className, open }) => {
    const [hidden, setHidden] = useState(true);
    // const [hidding, setHidding] = useState(false);
    useEffect(() => {
        if (!open) {
            // setTimeout(() => {
            //     setHidding(false);
            //     setHidden(true);
            // }, 400);
            setHidden(true);
        } else {
            setHidden(false);
            // setVisible(true);
        }
    }, [open]);
    return (
        <div
            className={classnames('Fade', className, {
                // hidding,
                hidden,
            })}
        >
            {children}
        </div>
    );
};

Fade.propTypes = {
    children: PropTypes.any.isRequired,
    className: PropTypes.string.isRequired,
    open: PropTypes.bool.isRequired,
    //
};

Fade.defaultProps = {};

export default Fade;
