import { useState, useEffect } from 'react';
import anime from 'animejs';
import PropTypes from 'prop-types';
import classnames from 'classnames';

const Pointer = ({ visible }) => {
    const [moving, setMoving] = useState(false);
    useEffect(() => {
        let timeout = null;
        document.addEventListener('mousemove', (e) => {
            setMoving(true);
            clearTimeout(timeout);
            timeout = setTimeout(() => setMoving(false), 300);
            anime({
                targets: '#pointer',
                top: e.clientY,
                left: e.clientX,
                duration: 0,
            });
        });
    }, []);

    return (
        <div id='pointer' className={classnames('Pointer', { moving, visible })}>
            <div className='PointerCenter' />
        </div>
    );
};

Pointer.propTypes = {
    visible: PropTypes.bool,
};

Pointer.defaultProps = {
    visible: false,
};

export default Pointer;
