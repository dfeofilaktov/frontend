import { useState } from 'react';
import classnames from 'classnames';

import PropTypes from 'prop-types';
import AdooInput from '../../../CustomShop/AdooInput/adoo_input';
import AdooButton from '../../../CustomShop/AdooButton/adoo_button';
import useGlobal from '../../../../hooks/store';
import coreone from '../../../../api/core';

const LoginForm = ({ open, onClose }) => {
    const [, actions] = useGlobal();
    const [response, setResp] = useState('no response');
    const [login, setLogin] = useState('');
    const [pass, setPass] = useState('');
    const ba = (resp) => {
        setResp(resp);
        if (resp.result.msg === 'ok') {
            actions.setLogged(true);
            onClose();
        } else {
            // console.log('blia');
        }
    };
    return (
        <div className='form_container'>
            <div className={classnames('first_plate', { open, closed: !open })} />
            <div className={classnames('second_plate', { open, closed: !open })} />
            <div className={classnames('main_plate', { open, closed: !open })}>
                <div className='sign_up'>
                    SIGN UP
                </div>
                <div className='transf_adoo_input'>
                    <AdooInput
                        // aaa={handleClick}
                        id='login'
                        inputType='text'
                        icon='person'
                        fakeText='Login'
                        onChange={(value) => setLogin(value)}
                    />
                    <AdooInput
                        // aaa={handleClick}
                        id='password'
                        inputType='password'
                        icon='lock'
                        fakeText='Password'
                        onChange={(value) => setPass(value)}
                    />
                    <div className={classnames('response', { red: true })}>
                        {response.msg}
                    </div>
                </div>
                <AdooButton
                    caption='Login'
                    onClick={() => {
                        coreone.dologin(login, pass)
                            .then((resp) => ba(resp.data))
                            .catch((error) => setResp(error.response.data || error));
                    }}
                />
            </div>
        </div>
    );
};

LoginForm.propTypes = {
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func,
};

LoginForm.defaultProps = {
    onClose: () => {},
};

export default LoginForm;
