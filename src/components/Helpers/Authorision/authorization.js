import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import classnames from 'classnames';
import LoginForm from './LoginForm/LoginForm';
import useGlobal from '../../../hooks/store';

const Authorization = ({ open }) => {
    if (!open) return null;
    const [, actions] = useGlobal();
    const [, setLoginOpened] = useState(false);

    const didMount = () => {
        setLoginOpened(true);
    };

    useEffect(didMount, []);

    const handleClick = () => {
        setLoginOpened(false);
        setTimeout(() => actions.setAuthorizationOpen(false), 500);
    };
    return (
        <div className={classnames('authorization', { closed: !open })}>
            <div
                role='presentation'
                className='empty_space'
                onClick={handleClick}
            />
            <LoginForm
                open={open}
                onClose={handleClick}
            />
            <div
                role='presentation'
                className={classnames('close', { closed: !open })}
                onClick={handleClick}
            >
                close
            </div>
        </div>
    );
};

Authorization.propTypes = {
    open: PropTypes.bool.isRequired,
};

Authorization.defaultProps = {};

export default Authorization;
