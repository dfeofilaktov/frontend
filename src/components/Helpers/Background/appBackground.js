import classnames from 'classnames';
import PropTypes from 'prop-types';
import { BACKGROUND_SRC, EFFECT_SRC } from '../../../constants/images';

const AppBackground = ({ darken }) => {
    return (
        <div className='AppBackground'>
            <div className='backgroundImage'><img src={BACKGROUND_SRC} alt='error' /></div>
            <div
                className={classnames('backgroundEffect', { darken })}
            >
                <img src={EFFECT_SRC} alt='error' />
            </div>
        </div>
    );
};

AppBackground.propTypes = {
    darken: PropTypes.bool,
};

AppBackground.defaultProps = {
    darken: false,
};

export default AppBackground;
