// import classnames from 'classnames';
import classnames from 'classnames';
import useGlobal from '../../../hooks/store';
// import Fade from '../Motions/fade';

// import PropTypes from 'prop-types';

const Loader = () => {
    const [store] = useGlobal();
    return (
        <div
            className={classnames('Loader', {
                // hidding,
                hidden: !store.loading,
            })}
        >
            Loading...
        </div>
    );
};

Loader.propTypes = {};

Loader.defaultProps = {};

export default Loader;
