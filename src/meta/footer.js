import * as componentsConst from '../constants/componentsConst';

const FooterDescription = [
    {
        name: componentsConst.FOOTER_RISIN,
        caption: 'Risin',
    },
    {
        name: componentsConst.FOOTER_RIVERSIDE,
        caption: 'Riverside',
    },
    {
        name: componentsConst.FOOTER_LIGHTHOUSE,
        caption: 'Lighthouse',
    },
];

export default FooterDescription;
