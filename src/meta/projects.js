// import * as componentsConst from '../constants/componentsConst';

import { TOPIC_ILLUSTRATIONS, TOPIC_WEB } from '../constants/componentsConst';

const topics = [
    {
        name: 'Web Sites',
        topic: TOPIC_WEB,
        img: 'https://i.imgur.com/H030a3i.jpg',
        description: 'We offer object and advertizing photo sessions and anything you want.',
    },
    {
        name: 'Illustrations',
        topic: TOPIC_ILLUSTRATIONS,
        img: 'https://i.imgur.com/H030a3i.jpg',
        description: 'We offer object and advertizing photo sessions and anything you want.',
    },
];

export default topics;
