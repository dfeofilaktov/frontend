import * as componentsConst from '../constants/componentsConst';

const MenuDescription = [
    {
        name: componentsConst.MENU_PORTFOLIO,
        caption: 'menu.portfolio',
    },
    {
        name: componentsConst.MENU_TEAM,
        caption: 'menu.team',
    },
];

export default MenuDescription;
