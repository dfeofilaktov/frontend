/* eslint-disable no-unused-vars */
/* eslint-disable no-param-reassign */
const canvas = () => {
    const mouse = {};
    const adoopoint = {};
    const points = [];
    const defaultColor = '200, 200, 200';
    const aroundPointsCount = 10;

    let context = null;
    let ref = null;
    let config = null;
    let outDivWidth = 0;
    let outDivHeight = 0;
    let blowed = false;

    const random = (min, max) => {
        return min + Math.random() * (max - min);
    };

    const getYDelta = (x) => {
        const a = x - adoopoint.xmin;
        return adoopoint.R
            - Math.sqrt((2 * adoopoint.R * a) - (a * a));
    };

    const getXDelta = (y) => {
        const a = y - adoopoint.ymin;
        return adoopoint.R
            - Math.sqrt((2 * adoopoint.R * a) - (a * a));
    };

    const randomPoints = () => {
        const { count, mouseDist, speed } = config;

        mouse.max = mouseDist;
        for (let i = 0; i < count; ++i) {
            const xRandom = random(adoopoint.xmin, adoopoint.xmax);
            const deltaY = getYDelta(xRandom);
            const yRandom = random(adoopoint.ymin + deltaY, adoopoint.ymax - deltaY);
            const xChill = random(0.5, 1) * speed;
            const yChill = random(0.5, 1) * speed;
            points.push({
                x: xRandom,
                y: yRandom,
                xStep: xChill * (Math.random() < 0.5 ? -1 : 1),
                yStep: yChill * (Math.random() < 0.5 ? -1 : 1),
                max: mouseDist,
            });
        }

        for (let i = 0; i <= aroundPointsCount; i++) {
            const alpha = (2 * Math.PI * i) / aroundPointsCount;
            points.push({
                x: adoopoint.x + adoopoint.R * Math.cos(alpha),
                y: adoopoint.y + adoopoint.R * Math.sin(alpha),
                xStep: 0,
                yStep: 0,
                max: mouseDist,
            });
        }

        points.push({
            x: adoopoint.x,
            y: adoopoint.y,
            xStep: 0,
            yStep: 0,
            max: mouseDist * 10,
        });
    };

    const setAdooPoint = () => {
        adoopoint.R = 80;
        adoopoint.x = outDivWidth / 2 - 350;
        adoopoint.y = outDivHeight / 2;
        adoopoint.xmin = adoopoint.x - adoopoint.R;
        adoopoint.xmax = adoopoint.x + adoopoint.R;
        adoopoint.ymin = adoopoint.y - adoopoint.R;
        adoopoint.ymax = adoopoint.y + adoopoint.R;

        adoopoint.color = '100, 100, 100';
    };

    const setSize = () => {
        const parent = ref.parentNode;

        outDivWidth = parent.clientWidth;
        outDivHeight = parent.clientHeight;

        ref.width = outDivWidth;
        ref.height = outDivHeight;
    };

    const drawFlower = () => {
        // Big adooone
        context.beginPath();
        const radgrad = context.createRadialGradient(
            adoopoint.x,
            adoopoint.y,
            adoopoint.R / 10,
            adoopoint.x,
            adoopoint.y,
            adoopoint.R,
        );
        radgrad.addColorStop(0, 'rgba(24,24,24,0)');
        radgrad.addColorStop(0.8, 'rgba(147,147,147,0.29)');
        radgrad.addColorStop(1, 'rgba(255,255,255,0.34)');
        context.fillStyle = radgrad;
        // context.fillStyle = `rgba(${adoopoint.color},${0.1})`;
        context.arc(
            adoopoint.x, adoopoint.y, adoopoint.R,
            0, 2 * Math.PI,
        );
        context.fill();

        // Center
        context.beginPath();
        context.fillStyle = `rgba(${defaultColor},${1})`;
        context.arc(
            adoopoint.x, adoopoint.y, adoopoint.R / 8,
            0, 2 * Math.PI,
        );
        context.fill();

        // Line
        context.beginPath();
        context.lineWidth = 10;
        const lingrad = context.createLinearGradient(
            adoopoint.x,
            adoopoint.y,
            adoopoint.x,
            outDivHeight,
        );
        lingrad.addColorStop(0, 'rgba(255,255,255,0)');
        lingrad.addColorStop(0.2, 'rgba(255,255,255,0.19)');
        lingrad.addColorStop(0.4, 'rgba(255,255,255,0)');
        lingrad.addColorStop(1, 'rgba(255,255,255,0)');
        // context.strokeStyle = `rgba(${lineColor},${0.1})`;
        context.strokeStyle = lingrad;
        context.moveTo(adoopoint.x, adoopoint.y);
        context.lineTo(adoopoint.x, outDivHeight);
        context.stroke();
    };

    const draw = () => {
        const {
            pointR, pointColor, pointOpacity, lineColor, lineWidth,
        } = config;

        context.clearRect(0, 0, outDivWidth, outDivHeight);

        for (let index = 0; index < points.length; ++index) {
            const point = points[index];

            const mouseXdist = point.x - mouse.x;
            const mouseYdist = point.y - mouse.y;
            const mouseDist = mouseXdist * mouseXdist + mouseYdist * mouseYdist;
            const mouseScale = (mouse.max - mouseDist * 5) / (mouse.max);

            context.beginPath();
            context.lineWidth = (mouseScale * lineWidth) / 2;
            context.strokeStyle = `rgba(${lineColor},${mouseScale})`;
            context.moveTo(point.x, point.y);
            context.lineTo(mouse.x, mouse.y);
            context.stroke();

            context.beginPath();
            context.fillStyle = `rgba(${pointColor},${pointOpacity})`;
            context.arc(point.x, point.y, pointR, 0, 2 * Math.PI);
            context.fill();

            point.x += point.xStep !== 0 ? point.xStep : 0;
            point.y += point.yStep !== 0 ? point.yStep : 0;

            if (!blowed) {
                const xmax = adoopoint.xmax - getXDelta(point.y);
                const xmin = adoopoint.xmin + getXDelta(point.y);
                const xCoef = (point.x + pointR >= xmax || point.x - pointR <= xmin)
                    ? -1 : 1;

                const ymax = adoopoint.ymax - getYDelta(point.x);
                const ymin = adoopoint.ymin + getYDelta(point.x);
                const yCoef = (point.y + pointR >= ymax || point.y - pointR <= ymin)
                    ? -1 : 1;

                point.xStep *= xCoef;
                point.yStep *= yCoef;
            } else {
                point.xStep *= (point.x + pointR >= outDivWidth || point.x - pointR <= 0)
                    ? -1 : 1;
                point.yStep *= (point.y + pointR >= outDivHeight || point.y - pointR <= 0)
                    ? -1 : 1;
            }

            for (let nextIndex = 0; nextIndex < points.length; ++nextIndex) {
                const nextPoint = points[nextIndex];
                const xDist = point.x - nextPoint.x;
                const yDist = point.y - nextPoint.y;
                const dist = xDist * xDist + yDist * yDist;
                const scale = (nextPoint.max - dist) / (nextPoint.max);

                context.beginPath();
                context.lineWidth = (scale * lineWidth) / 2;
                context.strokeStyle = `rgba(${lineColor},${scale})`;
                context.moveTo(point.x, point.y);
                context.lineTo(nextPoint.x, nextPoint.y);
                context.stroke();
            }
        }
    };

    const animate = () => {
        draw();
        if (!blowed) drawFlower();
        requestAnimationFrame(animate);
    };

    const init = (conf = {}) => {
        const {
            count = 60,
            mouseDist = 4000,
            speed = 0.5,
            pointR = 1,
            pointColor = defaultColor,
            pointOpacity = 1,
            lineColor = defaultColor,
            lineWidth = 0.5,
        } = conf;
        config = {
            count, mouseDist, speed, pointR,
            pointColor, pointOpacity, lineColor, lineWidth,
        };

        ref = document.getElementById('canvas');
        context = ref.getContext('2d');

        document.addEventListener('mousemove', (e) => {
            mouse.x = e.clientX;
            mouse.y = e.clientY;
        });

        setSize();
        setAdooPoint();
        randomPoints();
        animate();
    };

    const blowLeft = (speed, step) => {
        for (let index = 0; index < points.length; ++index) {
            const point = points[index];
            const xChill = random(0.4, 1) * speed;
            point.xStep = xChill * -1;
        }
        const newSpeed = speed - 0.05;
        // const newSpeed = speed - (5 * Math.sin((2 * Math.PI * step) / 100));
        const timeout = setTimeout(() => blowLeft(newSpeed, step + 1), 50);
        // console.log(speed);
        if (step > 100) {
            clearTimeout(timeout);
        }
    };

    const blow = () => {
        blowed = true;
        config.pointColor = '47, 198, 134';
        config.lineColor = '47, 198, 134';
        config.pointR = 1;
        config.lineWidth = 1;
        config.speed = 2;
        const { speed, mouseDist } = config;

        for (let index = 0; index < points.length; ++index) {
            const point = points[index];
            if (point.xStep === 0 || point.yStep === 0) {
                const xChill = random(0.1, 1) * speed;
                const yChill = random(0.1, 1) * speed;
                point.xStep = xChill * (Math.random() < 0.5 ? -1 : 1);
                // point.xStep = xChill * -1;
                point.yStep = yChill * (Math.random() < 0.5 ? -1 : 1);
                point.max = mouseDist * 5;
            }
        }
        // blowLeft(4, 0);
    };

    return {
        init,
        blow,
    };
};

export default canvas();
