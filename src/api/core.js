import axios from 'axios';
import * as env from '../constants/environment';

switch (process.env.CORE_HOST) {
case env.CORE_HOST_LOCAL: axios.defaults.baseURL = env.ADOOONE_LOCAL_HOST; break;
case env.CORE_HOST_STAGE: axios.defaults.baseURL = env.ADOOONE_STAGE_HOST; break;
case env.CORE_HOST_PROD: axios.defaults.baseURL = env.ADOOONE_PROD_HOST; break;
default: axios.defaults.baseURL = '';
}
// Chose backend host you want front communicate with and uncomment it
// axios.defaults.baseURL = env.ADOOONE_LOCAL_HOST; // Local Host: localhost:9090
// axios.defaults.baseURL = env.ADOOONE_STAGE_HOST; // Stage Host: adooone.space
// axios.defaults.baseURL = env.ADOOONE_PROD_HOST; // Production Host: adooone.com

const coreone = (() => {
    const ping = () => {
        // return axios.get('/core/ping', { });
        return new Promise(() => {});
    };

    const dologin = (/* login, password */) => {
        // return axios.post('/core/users/login', { data: { login, password } });
        return new Promise(() => {});
    };

    const getProjects = () => {
        // return axios.get('/core/get_projects');
        return new Promise(() => {});
    };

    return {
        ping,
        dologin,
        getProjects,
    };
})();

export default coreone;
