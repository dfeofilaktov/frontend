const axios = require('axios');
const { name } = require('../package.json');

(async function deploy() {
    try {
        const res = await axios.post('http://adooone.com:8633/api/deploy_static', {
            src: 'https://dfeofilaktov@bitbucket.org/dfeofilaktov/frontend.git',
            name,
        });
        console.log(`${res.data.response}\n`);
        return res;
    } catch (e) {
        throw new Error(e.message);
    }
}());
